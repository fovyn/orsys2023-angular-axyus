import { Component } from '@angular/core';
import {BlopService} from "../services/blop.service";

@Component({
  selector: 'axyus-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss'],
  providers: [ BlopService ]
})
export class CoreComponent {

}
