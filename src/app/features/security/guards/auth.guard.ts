import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";
import {Store} from "@ngrx/store";
import {selectSecurityToken} from "../store/security.selector";
import {filter, map} from "rxjs";

export const authGuard: CanActivateFn = (route, state) => {
  const $store = inject(Store)
  const $router = inject(Router)

  return $store.select(selectSecurityToken).pipe(
    map((token) => {
      if (token) return true

      return $router.createUrlTree(["/signIn"])
    })
  );
};


// @Injectable()
// export class AuthGuard implements CanActivate {
//   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
//     return undefined;
//   }
//
// }
