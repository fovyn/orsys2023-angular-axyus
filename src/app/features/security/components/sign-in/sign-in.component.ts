import { Component } from '@angular/core';
import {fSignIn} from "../../models/security.form";
import {Store} from "@ngrx/store";
import {SecurityActions} from "../../store/security.action";

@Component({
  selector: 'axyus-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  private form = fSignIn()
  get Form() { return this.form }

  constructor(
    private $store: Store
  ) {
  }


  handleSubmit() {
    if (this.Form.valid) {
      const { email, password } = this.Form.value

      this.$store.dispatch(SecurityActions.signIn({ username: email, password }))
    }
  }
}
