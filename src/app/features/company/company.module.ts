import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyComponent } from './container/company.component';
import { CompanyListComponent, CompanyDetailComponent, CompanyCreateComponent } from './components';
import { CompanyService } from "./services/company.service";
import {MaterializeCssModule} from "../../shared/materialize-css/materialize-css.module";
import {TranslateModule} from "@ngx-translate/core";
import {TypedFormModule} from "../../shared/typed-form/typed-form.module";
import {StoreModule} from "@ngrx/store";
import {COMPANIES_FEATURE} from "./store/companies.state";
import {companiesReducer} from "./store/companies.reducer";
import {EffectsModule} from "@ngrx/effects";
import {CompaniesEffect} from "./store/companies.effect";


@NgModule({
  declarations: [
    CompanyComponent,
    CompanyListComponent,
    CompanyDetailComponent,
    CompanyCreateComponent
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    MaterializeCssModule,
    TypedFormModule,
    TranslateModule,
    StoreModule.forFeature(COMPANIES_FEATURE, companiesReducer),
    EffectsModule.forFeature([CompaniesEffect])
  ],
  providers: [CompanyService],
})
export class CompanyModule { }
