import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TypedGroup} from "../../../shared/typed-form/models/typed-group";
import {TypedArray} from "../../../shared/typed-form/models/typed-array";

export type FCompanyCreate = {
  name: FormControl
}

export function fCompanyCreate(): TypedGroup<FCompanyCreate> {
  return new TypedGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(1)]),
  })
}
