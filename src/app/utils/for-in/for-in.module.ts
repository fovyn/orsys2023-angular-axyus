import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForInDirective } from './directives/for-in.directive';



@NgModule({
  declarations: [
    ForInDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [ForInDirective]
})
export class ForInModule { }
