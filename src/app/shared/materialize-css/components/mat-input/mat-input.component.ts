import {Component, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

export enum InputType {
  text= "text",
  password= "password",
  number= "number"
}

@Component({
  selector: 'mat-input',
  templateUrl: './mat-input.component.html',
  styleUrls: ['./mat-input.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MatInputComponent, multi: true }
  ]
})
export class MatInputComponent implements ControlValueAccessor {
  @Input("id") id: string = ""
  @Input("name") name: string = ""
  @Input("label") label: string = ""
  @Input("disabled") disabled: boolean = false
  @Input('type') type: InputType = InputType.text

  value: any

  onChangeFn: any
  onTouchedFn: any

  registerOnChange(fn: any): void {
    this.onChangeFn = fn
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled
  }

  writeValue(obj: any): void {
    this.value = obj
  }

  handleInput({value}: any) {
    this.onChangeFn(value)
  }
}
