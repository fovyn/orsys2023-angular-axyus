import {createFeatureSelector, createSelector} from "@ngrx/store";
import {CONFIG_FEATURE, ConfigState} from "./config.state";

const feature = createFeatureSelector<ConfigState>(CONFIG_FEATURE)

export const selectConfigBaseUri = createSelector(
  feature,
  (state) => state.base_uri
)

export const selectConfigRedirect = createSelector(
  feature,
  (state) => state.home_redirect
)
