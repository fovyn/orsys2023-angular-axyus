import {AfterViewInit, Directive, ElementRef, HostBinding, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import * as M from 'materialize-css'
@Directive({
  selector: '[matCollapsible]'
})
export class MatCollapsibleDirective implements AfterViewInit, OnDestroy {
  @HostListener('click') onClick() {
    console.log()

    return false;
  }
  @HostListener("mouseover") onMouseOver() {
    return false;
  }

  @HostBinding('style.backgroundColor') color = "#c21616"

  private options: Partial<M.CollapsibleOptions> = {}
  private instance: M.Collapsible | undefined
  @Input("options")
  set Options(v: Partial<M.CollapsibleOptions>) {
    this.options = {...this.options, ...v}
  }

  constructor(
    private $er: ElementRef<HTMLUListElement>
  ) { }

  ngAfterViewInit() {
    this.instance = M.Collapsible.init(this.$er.nativeElement, {...this.options})
  }

  open(id: number) {
    this.color = "#FFF"
    this.instance?.open(id)
  }
  close(id: number) {
    this.instance?.close(id)
  }

  ngOnDestroy(): void {
    this.instance?.destroy()
  }
}
