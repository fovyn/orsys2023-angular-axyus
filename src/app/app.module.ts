import {APP_INITIALIZER, isDevMode, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterializeCssModule} from "./shared/materialize-css/materialize-css.module";
import {ForInModule} from "./utils/for-in/for-in.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {Store, StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {ConfigModule} from "./shared/config/config.module";
import {ConfigActions} from "./shared/config/store/config.action";
import {environment} from "../environments/environment";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import { SecurityModule } from './features/security/security.module';

function initConfig($store: Store) {
  return async () => {
    $store.dispatch(ConfigActions.load({ endpoint: `/assets/config/${environment.mode}.config.json`}))
  }
}

export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http)
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: { provide: TranslateLoader, useFactory: httpTranslateLoader, deps: [HttpClient] }
    }),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: !isDevMode()}),
    ConfigModule,
    SecurityModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: ($store: Store) => initConfig($store),
      deps: [Store],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
