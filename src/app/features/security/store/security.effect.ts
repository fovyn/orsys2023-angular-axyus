import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {AuthService} from "../services/auth.service";
import {SecurityActions} from "./security.action";
import {catchError, map, of, switchMap, tap} from "rxjs";
import {Router} from "@angular/router";
import {ConfigActions} from "../../../shared/config/store/config.action";
import {Store} from "@ngrx/store";
import {selectConfigRedirect} from "../../../shared/config/store/config.selector";

@Injectable()
export class SecurityEffect {

  $signInAction = createEffect(() => this.$actions.pipe(
    ofType(SecurityActions.signIn),
    switchMap(({ username, password }) => this.$auth.signIn(username, password)),
    map(({token, user}) => SecurityActions.signInSuccess({ token, user })),
    catchError((err) => of(SecurityActions.signInFailed({err})))
  ))

  $signInSuccessAction = createEffect(() => this.$actions.pipe(
    ofType(SecurityActions.signInSuccess),
    switchMap(() => this.$store.select(selectConfigRedirect)),
    tap(uri => this.$router.navigate([uri]))
  ), {dispatch: false})

  constructor(
    private $actions: Actions,
    private $auth: AuthService,
    private $router: Router,
    private $store: Store
  ) {
  }
}
