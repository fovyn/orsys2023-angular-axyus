import {createActionGroup, props} from "@ngrx/store";

export const SecurityActions = createActionGroup({
  source: "Security",
  events: {
    signIn: props<{ username: string, password: string }>(),
    signInSuccess: props<{ token: string, user: any}>(),
    signInFailed: props<{err: any}>()
  }
})
