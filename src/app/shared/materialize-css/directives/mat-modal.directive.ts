import {Directive, ElementRef, Input} from '@angular/core';
import * as M from 'materialize-css'
@Directive({
  selector: '[matModal]'
})
export class MatModalDirective {
  private instance: M.Modal | undefined
  private options: Partial<M.ModalOptions> = {}

  @Input("options")
  set Options(v: Partial<M.ModalOptions>) {
    this.options = {...this.options, ...v }
  }

  constructor(private $er: ElementRef<HTMLDivElement>) {
  }

  ngAfterViewInit() {

    this.instance = M.Modal.init(this.$er.nativeElement, { ...this.options })
  }

  open() {
    this.instance?.open()
  }

  close() {
    this.instance?.close()
  }
}
