import { Component } from '@angular/core';
import {CompanyService} from "../../services/company.service";
import {Store} from "@ngrx/store";
import {filter, map, Observable, of, switchMap} from "rxjs";
import {Company} from "../../models/company.model";
import {selectCompany} from "../../store/companies.selector";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent {
  company$: Observable<Company | undefined>

  constructor(
    private $route: ActivatedRoute,
    private $store: Store
  ) {
    this.company$ = $route.paramMap.pipe(
      map(map => map.get("id")),
      filter((id) => id !== null),
      switchMap(id => this.$store.select(selectCompany(parseInt(id!))))
    )
  }
}
