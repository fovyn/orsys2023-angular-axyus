import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CompanyService, Page} from "../../services/company.service";
import {Company} from "../../models/company.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {
  selectCompanies,
  selectCompaniesLoading,
  selectCompaniesTotal,
  selectState
} from "../../store/companies.selector";
import {Observable} from "rxjs";
import {CompaniesActions} from "../../store/companies.action";

@Component({
  selector: 'company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {
  loading$: Observable<Boolean>

  @Output('detail') detailEvent = new EventEmitter<Company>()
  private size: number = 5

  get Size(): number { return this.size }
  set Size(v: number) {
    this.size = v
    this.$store.dispatch(CompaniesActions.load({page: 1, limit: v}))
  }

  set Page(v: number) {
    this.$store.dispatch(CompaniesActions.load({page: v, limit: this.size}))
  }


  items$: Observable<Company[]>
  total$: Observable<number>

  page$: Observable<Page<Company>>
  constructor(
    private $route: ActivatedRoute,
    private $router: Router,
    private $store: Store
  ) {
    this.loading$ = $store.select(selectCompaniesLoading)
    this.items$ = $store.select(selectCompanies)
    this.total$ = $store.select(selectCompaniesTotal)

    this.page$ = $store.select(selectState)
  }

  ngOnInit() {
    this.$store.dispatch(CompaniesActions.init())
  }
  handleRemove(item: Company) {
    this.$store.dispatch(CompaniesActions.remove({id: item.id}))
  }

  handleDetails(item: Company) {
    this.$router.navigate(['./', item.id], { relativeTo: this.$route })
  }
}
