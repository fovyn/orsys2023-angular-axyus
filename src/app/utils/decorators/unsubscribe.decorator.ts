import {Subscription} from "rxjs";

export function Unsubscribe(): ClassDecorator {
  return (constructor) => {
    const origin = constructor.prototype.ngOnDestroy

    constructor.prototype.ngOnDestroy = function () {
      for(let attrKey in this) {
        const attr = this[attrKey]
        if (attr instanceof Subscription) {
          attr.unsubscribe()
        }
      }

      if (origin) {
        origin()
      }
    }
  }
}
