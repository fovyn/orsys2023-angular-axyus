import {createReducer, on} from "@ngrx/store";
import {initialState} from "./companies.state";
import {CompaniesActions} from "./companies.action";

export const companiesReducer = createReducer(
  initialState,
  on(CompaniesActions.load, (state) => ({
    ...state,
    loading: true
  })),
  on(CompaniesActions.loadSuccess, (state, {data, total}) => ({
    ...state,
    data,
    total,
    loading: false
  })),
  on(CompaniesActions.remove, (state, { id }) => {
    const data = [...state.data]
    const index = data.findIndex(it => it.id === id)

    data.splice(index, 1)

    return { ...state, data, total: state.total - 1 }
  }),
  on(CompaniesActions.createSuccess, (state, { company }) => ({
    ...state,
    data: [...state.data, company],
    total: state.total + 1
  }))
)
