import { ComponentFixture, TestBed } from '@angular/core/testing'
import { SignInComponent } from "./sign-in.component";
import {reduceState, StoreModule} from "@ngrx/store";
import {SECURITY_FEATURE} from "../../store/security.state";
import {securityReducer} from "../../store/security.reducer";
import {RegisterComponent} from "../register/register.component";
import {CommonModule} from "@angular/common";
import {SecurityRoutingModule} from "../../security-routing.module";
import {EffectsModule} from "@ngrx/effects";
import {SecurityEffect} from "../../store/security.effect";
import {TypedFormModule} from "../../../../shared/typed-form/typed-form.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "../../interceptors/token.interceptor";
import {httpTranslateLoader} from "../../../../app.module";
import {TestScheduler} from "rxjs/internal/testing/TestScheduler";
import {of} from "rxjs";


describe('SignInComponent', () => {
  let component: SignInComponent
  let fixture: ComponentFixture<SignInComponent>

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SignInComponent,
        RegisterComponent
      ],
      imports: [
        CommonModule,
        SecurityRoutingModule,
        HttpClientModule,
        StoreModule.forRoot(reduceState),
        EffectsModule.forRoot([SecurityEffect]),
        TypedFormModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useFactory: httpTranslateLoader, deps: [HttpClient] }
        }),
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        }
      ]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should form be valid', () => {
    const button = fixture.nativeElement.querySelector("button")
    component.Form.patchValue({ email: 'flavian.ovyn@bstorm.be', password: 'Test1234='})
    button.click()

    expect(component.Form.valid).toBeTrue()
  })

  it('should form not valid', () => {
    const button = fixture.nativeElement.querySelector("button")
    component.Form.patchValue({ email: 'flavian.ovy.be', password: 'Test1234='})
    button.click()

    console.log(component.Form.value)

    expect(component.Form.valid).toBeFalse()

    const testScheduler = new TestScheduler((actual, expect) => {

    })
    testScheduler.run(({ expectObservable }) => {
      const source$ = of({ a: { userId: 1, uri: 'http://localhost:3000'}})
    })
  })
})

