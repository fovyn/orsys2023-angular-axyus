import {Company} from "../models/company.model";

export interface CompaniesState {
  data: Company[]
  loading: boolean,
  total: number
}

export const initialState: CompaniesState = {
  data: [],
  loading: false,
  total: 0,
}

export const COMPANIES_FEATURE = "companies"
