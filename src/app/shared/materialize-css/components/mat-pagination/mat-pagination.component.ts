import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'mat-pagination',
  templateUrl: './mat-pagination.component.html',
  styleUrls: ['./mat-pagination.component.scss']
})
export class MatPaginationComponent {
  private currentPage = 1
  private maxPage = 1
  private total = 0
  private pageSize = 5

  @Output("page") pageEvent = new EventEmitter<number>()
  @Output("pageSizeChange") pageSizeEvent = new EventEmitter<number>()

  get CurrentPage(): number { return this.currentPage }
  get Pages(): number[] {
    const mod = this.total % this.pageSize
    const nbPage = Math.floor(this.total / this.pageSize) + (mod > 0 ? 1 : 0)

    const pages = []
    for(let i = 1; i <= nbPage; i++) {
      pages.push(i)
    }

    this.maxPage = nbPage

    return pages;
  }
  @Input("total")
  set Total(v: number) {
    this.total = v
  }

  @Input("pageSize")
  set PageSize(v: number) {
    this.pageSize = v
  }

  goToPage(page: number) {
    this.currentPage = page
    this.pageEvent.emit(this.currentPage)
  }

  gotToNext() {
    if(this.currentPage + 1 > this.maxPage) return
    ++this.currentPage
    this.pageEvent.emit(this.currentPage)
  }

  goToPrevious() {
    if (this.currentPage - 1 < 1) return
    --this.currentPage
    this.pageEvent.emit(this.currentPage)
  }
}
