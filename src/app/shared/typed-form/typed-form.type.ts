import {AbstractControl} from "@angular/forms";

export type TypedFormDeclaration = { [key:string]: AbstractControl }
