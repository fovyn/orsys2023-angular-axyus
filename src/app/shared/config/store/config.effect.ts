import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {ConfigService} from "../services/config.service";
import {ConfigActions} from "./config.action";
import {catchError, exhaustMap, map, of} from "rxjs";

@Injectable()
export class ConfigEffect {

  $loadAction = createEffect(() => this.$actions.pipe(
    ofType(ConfigActions.load),
    exhaustMap(({ endpoint }) => this.$config.load(endpoint)),
    map((value) => ConfigActions.loadSuccess({ config: value })),
    catchError((err, caught) => of(ConfigActions.loadFailed({err})))
  ))

  constructor(
    private $actions: Actions,
    private $config: ConfigService
  ) {
  }
}
