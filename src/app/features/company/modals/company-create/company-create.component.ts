import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import {Company} from "../../models/company.model";
import {FCompanyCreate, fCompanyCreate} from "../../models/company.form";
import {TypedGroup} from "../../../../shared/typed-form/models/typed-group";
import {Store} from "@ngrx/store";
import {CompaniesActions} from "../../store/companies.action";
import {MatModalDirective} from "../../../../shared/materialize-css/directives/mat-modal.directive";
import {InputType} from "../../../../shared/materialize-css/components/mat-input/mat-input.component";

@Component({
  selector: 'company-create',
  templateUrl: './company-create.component.html',
  styleUrls: ['./company-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompanyCreateComponent {
  @ViewChild(MatModalDirective) modal: MatModalDirective |undefined

  @Output("close") closeEvent = new EventEmitter()
  @Output('create') createEvent = new EventEmitter<Company>()

  modalOptions: { title: string } = {title: 'MODAL'}
  @Input('modalOptions')
  set ModalOptions(v: {title: string}) {
    this.modalOptions = { ...v }
  }

  options: Partial<M.ModalOptions> = { onCloseEnd: (el) => this.close() }

  type = InputType

  private form = fCompanyCreate()
  get Form(): TypedGroup<FCompanyCreate> { return this.form }

  constructor(
    private $store: Store,
    private $cdr: ChangeDetectorRef
  ) {
  }

  ngAfterViewInit() {
    this.modal?.open()
  }

  handleSubmit() {
    if (this.Form.valid) {
      this.$store.dispatch(CompaniesActions.create({ company: this.Form.value }))
    }
  }

  close() {
    this.modal?.close();
    this.closeEvent.emit()
  }
}
