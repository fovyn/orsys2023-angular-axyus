export type ForInContext = {
  $implicit: any,
  value: any,
  isLast: boolean
}
