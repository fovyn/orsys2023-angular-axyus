import {
  AbstractControl,
  AbstractControlOptions,
  AsyncValidatorFn, FormArray,
  FormControl,
  FormGroup,
  ValidatorFn
} from "@angular/forms";
import {TypedFormDeclaration} from "../typed-form.type";
import {TypedArray} from "./typed-array";

export class TypedGroup<T extends TypedFormDeclaration> extends FormGroup {


  constructor(
    controls: T,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  private getAbstract(key: keyof T): AbstractControl {
    const ac = this.get(key.toString())
    if (!ac) throw new Error(`Field ${key.toString()} doesn't exists`)

    return ac
  }

  getControl(key: keyof T): FormControl {
    const ac = this.getAbstract(key)
    if (!(ac instanceof FormControl)) throw new Error(`Field is not a FormControl`)

    return ac as FormControl
  }

  getArray<U extends TypedFormDeclaration>(key: keyof T): TypedArray<U> {

    const ac = this.getAbstract(key)
    if (!(ac instanceof FormArray)) throw new Error(`Field is not a FormArray`)

    return ac as TypedArray<U>
  }
}
