export interface SecurityState {
  token: string | null
  user: any | undefined,
  error: any | undefined
}

export const initialState: SecurityState = {
  token: null,
  user: undefined,
  error: undefined
}

export const SECURITY_FEATURE = "security"
