import {ApplicationRef, createComponent, EmbeddedViewRef, Injectable, Type} from '@angular/core';

@Injectable()
export class ModalService {

  constructor(
    private $appRef: ApplicationRef
  ) { }

  open<T>(component: Type<T>, opts?: any) {
    const componentRef = createComponent(component, {environmentInjector: this.$appRef.injector})

    this.$appRef.attachView(componentRef.hostView)

    document.body.append((<EmbeddedViewRef<T>>componentRef.hostView).rootNodes[0])

    return componentRef
  }
}
