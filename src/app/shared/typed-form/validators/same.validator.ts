import {AbstractControl, FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {TypedGroup} from "../models/typed-group";

export function same<T>(field1: keyof T, field2: keyof T): ValidatorFn {
  return (ac: AbstractControl) => {
    if (!(ac instanceof FormGroup)) return { 'type': 'Control type mismatch' }
    const tf = ac as TypedGroup<any>

    const { value: f1Value } = tf.getControl(field1)
    const { value: f2Value} = tf.getControl(field2)


    if (f1Value !== f2Value) return { 'same': `Field are not the same`}

    return null
  }
}

export function domain(domain: RegExp): ValidatorFn {
  return (ac: AbstractControl) => {
    if (!(ac instanceof FormControl)) return { 'type': 'Control type mismatch' }
    const fc = ac as FormControl

    const { value } = fc

    const res = domain.exec(value)

    if (!res) {
      return { 'domain': `Email doesn't below the domain`}
    }

    return null

  }
}
