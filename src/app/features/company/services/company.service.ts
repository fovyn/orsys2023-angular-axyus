import {Injectable, signal} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {map, Observable, shareReplay, switchMap, takeLast, tap} from "rxjs";
import {Company, CompanyForm} from "../models/company.model";
import {Store} from "@ngrx/store";
import {selectConfigBaseUri} from "../../../shared/config/store/config.selector";

export interface Page<T> {
  total: number
  data: T[]
}

@Injectable()
export class CompanyService {
  items = signal<Company[]>([])
  toUpdate = signal<Company | null>(null)

  constructor(private $http: HttpClient, private $store: Store) { }

  create(company: CompanyForm) {
    return this.$store.select(selectConfigBaseUri).pipe(
      switchMap((base_uri) => this.$http.post<Company>(`${base_uri}/companies`, {...company})),
    )
  }

  getAll(page = 1, limit = 5): Observable<Page<Company>> {
    const params = new HttpParams().appendAll({ "_page": page, "_limit": limit })

    return this.$store.select(selectConfigBaseUri).pipe(
      switchMap((base_uri) => this.$http.get<Company[]>(`${base_uri}/companies`, { params, observe: 'response' })),
      map(res => {
        const { headers, body } = res
        const total = parseInt(headers.get('X-Total-Count')!)

        return { total, data: body || []}
      })
    )
  }

  remove(id: any) {
    return this.$store.select(selectConfigBaseUri).pipe(
      switchMap((base_uri) => this.$http.delete<any>(`${base_uri}/companies/${id}`))
    )
  }
}
