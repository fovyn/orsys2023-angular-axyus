import {createReducer, on} from "@ngrx/store";
import {initialState} from "./config.state";
import {ConfigActions} from "./config.action";

export const configReducer = createReducer(
  initialState,
  on(ConfigActions.loadSuccess, (state, {config}) => ({ ...state, ...config}))
)
