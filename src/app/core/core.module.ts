import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './container/core.component';
import { HomeComponent } from './components/home/home.component';
import {BlopService} from "./services/blop.service";
import {MaterializeCssModule} from "../shared/materialize-css/materialize-css.module";
import { MenuComponent } from './components/menu/menu.component';
import { DemoComponent } from './components/demo/demo.component';


@NgModule({
  declarations: [
    CoreComponent,
    HomeComponent,
    MenuComponent,
    DemoComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    MaterializeCssModule
  ],
  providers: [BlopService]
})
export class CoreModule { }
