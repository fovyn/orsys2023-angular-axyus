import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpInterceptorFn,
  HttpRequest
} from '@angular/common/http';
import {inject, Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {selectSecurityToken} from "../store/security.selector";
import {map, Observable, switchMap} from "rxjs";

// export const tokenInterceptor: HttpInterceptorFn = (req, next) => {
//   const $store = inject(Store)
//
//   return $store.select(selectSecurityToken).pipe(
//     switchMap(token => {
//       const cpy = req.clone({ setHeaders: { "Authorization": "Bearer "+ token } })
//
//       return next(cpy)
//     })
//   );
// };

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private $store: Store
  ) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.$store.select(selectSecurityToken).pipe(
      switchMap(token => {
        const cpy = req.clone({ setHeaders: { "Authorization" : "Bearer "+ token  ||"" } })

        return next.handle(cpy)
      })
    );
  }


}
