import {createActionGroup, emptyProps, props} from "@ngrx/store";
import {Company, CompanyForm} from "../models/company.model";

export const CompaniesActions = createActionGroup({
  source: 'Companies',
  events: {
    init: emptyProps(),
    load: props<{ page: number, limit: number}>(),
    loadSuccess: props<{ data: Company[], total: number }>(),

    remove: props<{ id: number }>(),
    removeSuccess: emptyProps(),

    create: props<{ company: CompanyForm }>(),
    createSuccess: props<{ company: Company }>(),

    selectById: props<{ id: number}>()
  }
})
