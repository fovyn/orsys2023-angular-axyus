import {CanActivateChildFn, CanActivateFn} from '@angular/router';
import {inject} from "@angular/core";
import {Store} from "@ngrx/store";
import {AuthService} from "../services/auth.service";
import {map} from "rxjs";

export function roleGuard(role: string): CanActivateChildFn {
  return (route, state) => {

    const $auth = inject(AuthService)

    return $auth.getRoles().pipe(
      map((roles) => roles.find(it => it.name === role) != null)
    );
  };
}
