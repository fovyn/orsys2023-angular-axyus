import {AfterViewInit, Directive, ElementRef} from '@angular/core';
import * as M from 'materialize-css'

@Directive({
  selector: '[matTabs]'
})
export class MatTabsDirective implements AfterViewInit {

  constructor(
    private $er: ElementRef<HTMLUListElement>
  ) { }

  ngAfterViewInit() {
    M.Tabs.init(this.$er.nativeElement)
  }
}
