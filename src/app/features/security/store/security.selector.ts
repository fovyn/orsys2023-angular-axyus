import {createFeatureSelector, createSelector} from "@ngrx/store";
import {SECURITY_FEATURE, SecurityState} from "./security.state";
import {selectConfigBaseUri} from "../../../shared/config/store/config.selector";

const feature = createFeatureSelector<SecurityState>(SECURITY_FEATURE)

export const selectSecurityToken = createSelector(
  feature,
  (state) => state.token
)

export const selectSecurityUser = createSelector(
  feature,
  (state) => state.user
)

export const selectSecurityUserAndBaseUri = createSelector(
  feature,
  selectConfigBaseUri,
  (state, uri) => ({uri, userId: state.user?.id})
)
