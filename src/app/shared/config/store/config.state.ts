export interface ConfigState {
  base_uri: string,
  home_redirect: string
}

export const initialState: ConfigState = {
  base_uri: '',
  home_redirect: ''
}

export const CONFIG_FEATURE = "config"
