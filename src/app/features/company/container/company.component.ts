import {Component, createComponent, ViewChild} from '@angular/core';
import {Company} from "../models/company.model";
import {CompanyCreateComponent, CompanyListComponent} from "../components";
import {ModalService} from "../../../shared/materialize-css/services/modal.service";

@Component({
  selector: 'axyus-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent {
  @ViewChild(CompanyListComponent) list: CompanyListComponent | undefined
  @ViewChild(CompanyCreateComponent) create: CompanyCreateComponent | undefined

  constructor(
    private $modal: ModalService,
  ) {
  }
  ngOnInit() {
  }

  ngAfterViewInit() {

  }

  openModal() {
    const ref = this.$modal.open(CompanyCreateComponent)

    ref.setInput("modalOptions", {title: 'Company Create'})
    const eventSubscription = ref.instance.closeEvent.subscribe(() => {
      ref.destroy()
      eventSubscription.unsubscribe()
    })
  }
}
