import {createReducer, on} from "@ngrx/store";
import {initialState} from "./security.state";
import {SecurityActions} from "./security.action";

export const securityReducer = createReducer(
  initialState,
  on(SecurityActions.signInSuccess, (state, { token, user}) => ({ ...state, token, user })),
  on(SecurityActions.signInFailed, (state, { err }) => ({ ...state, error: err }))
)
