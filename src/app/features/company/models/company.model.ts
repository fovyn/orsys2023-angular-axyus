
export interface Company {
  id: number
  name: string
}

export type CompanyForm = Omit<Company, 'id'>

