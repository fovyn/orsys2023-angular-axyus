import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyComponent } from './container/company.component';
import {CompanyCreateComponent, CompanyDetailComponent} from "./components";
import {roleGuard} from "../security/guards/role.guard";

const routes: Routes = [
  {
    path: '',
    component: CompanyComponent,
    children: [
      // { path: '', component: CompanyCreateComponent },
      { path: ':id', component: CompanyDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
