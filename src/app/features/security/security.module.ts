import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityRoutingModule } from './security-routing.module';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { RegisterComponent } from './components/register/register.component';
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {SECURITY_FEATURE} from "./store/security.state";
import {securityReducer} from "./store/security.reducer";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {TokenInterceptor} from "./interceptors/token.interceptor";
import {SecurityEffect} from "./store/security.effect";
import {TypedFormModule} from "../../shared/typed-form/typed-form.module";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    SignInComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    SecurityRoutingModule,
    StoreModule.forFeature(SECURITY_FEATURE, securityReducer),
    EffectsModule.forFeature([SecurityEffect]),
    TypedFormModule,
    TranslateModule
  ],
  exports: [
    SignInComponent,
    RegisterComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class SecurityModule { }
