import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'core-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  constructor(
    private $translate: TranslateService
  ) {
  }

  changeLang($event: any) {
    this.$translate.use($event)
  }
}
