import {AfterViewInit, Component, ContentChild, ElementRef} from '@angular/core';
import {MatCollapsibleDirective} from "../../../shared/materialize-css/directives/mat-collapsible.directive";
import {SecurityActions} from "../../../features/security/store/security.action";
import {Store} from "@ngrx/store";

@Component({
  selector: 'axyus-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {

  constructor(private $store: Store) {
  }
  ngAfterViewInit() {
  }
}
