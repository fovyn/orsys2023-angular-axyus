import {createActionGroup, props} from "@ngrx/store";
import {ConfigState} from "./config.state";

export const ConfigActions = createActionGroup({
  source: 'Config',
  events: {
    load: props<{ endpoint: string}>(),
    loadSuccess: props<{ config: ConfigState}>(),
    loadFailed: props<{ err: any}>()
  }
})
