import {createFeatureSelector, createSelector} from "@ngrx/store";
import {COMPANIES_FEATURE, CompaniesState} from "./companies.state";
import {Page} from "../services/company.service";
import {Company} from "../models/company.model";

const feature = createFeatureSelector<CompaniesState>(COMPANIES_FEATURE)

export const selectCompanies = createSelector(
  feature,
  (state) => state.data
)

export const selectCompaniesLoading = createSelector(
  feature,
  (state) => state.loading
)

export const selectCompaniesTotal = createSelector(
  feature,
  (state) => state.total
)

export const selectCompany = (id: number) => createSelector(
  selectCompanies,
  (companies) => companies.find(it => it.id === id)
)

export const selectState = createSelector(
  selectCompanies,
  selectCompaniesTotal,
  (companies, total) => ({ data: companies, total } as Page<Company>)
)
