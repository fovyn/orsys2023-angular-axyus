import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCollapsibleDirective } from './directives/mat-collapsible.directive';
import { MatTabsDirective } from './directives/mat-tabs.directive';
import { MatSelectDirective } from './directives/mat-select.directive';
import { MatPaginationComponent } from './components/mat-pagination/mat-pagination.component';
import {ModalService} from "./services/modal.service";
import { MatModalDirective } from './directives/mat-modal.directive';
import { MatInputComponent } from './components/mat-input/mat-input.component';



@NgModule({
  declarations: [
    MatCollapsibleDirective,
    MatTabsDirective,
    MatSelectDirective,
    MatPaginationComponent,
    MatModalDirective,
    MatInputComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    ModalService
  ],
  exports: [
    MatPaginationComponent,
    MatCollapsibleDirective,
    MatTabsDirective,
    MatSelectDirective,
    MatModalDirective,
    MatInputComponent
  ]
})
export class MaterializeCssModule { }
