import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngrx/store";
import {SecurityActions} from "./features/security/store/security.action";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'orsys2023-axyus';

  constructor(
    private $translate: TranslateService
  ) {
    $translate.addLangs(["fr", "en"])
    $translate.setDefaultLang("fr")
  }

  useFr() {
    this.$translate.use('fr')
  }
}
