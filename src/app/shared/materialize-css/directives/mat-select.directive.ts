import {Directive, ElementRef, EventEmitter, Output} from '@angular/core';
import * as M from 'materialize-css'
import {TranslateService} from "@ngx-translate/core";
@Directive({
  selector: '[matSelect]'
})
export class MatSelectDirective {
  @Output("mat-change") changeEvent = new EventEmitter<any>()

  constructor(
    private $er: ElementRef<HTMLSelectElement>,
  ) {

  }


  ngAfterViewInit() {
    this.$er.nativeElement.addEventListener('change', () => this.changeEvent.emit(this.$er.nativeElement.value))
    M.FormSelect.init(this.$er.nativeElement)
  }
}
