import {AfterViewInit, Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {ForInContext} from "./for-in.context";

@Directive({
  selector: '[for]'
})
export class ForInDirective implements OnInit {
  @Input('forIn') obj: any

  constructor(
    private $tr: TemplateRef<ForInContext>,
    private $vcr: ViewContainerRef
  ) { }

  ngOnInit() {
    for(let key in this.obj) {
      this.$vcr.createEmbeddedView(this.$tr, { $implicit: key, value: this.obj[key], isLast: false })
    }
  }
}
