import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoreComponent } from './container/core.component';
import {HomeComponent} from "./components/home/home.component";
import {authGuard} from "../features/security/guards/auth.guard";

const routes: Routes = [
  {
    path: '',
    component: CoreComponent,
    children: [
      { path: '', component: HomeComponent },
      {
        path: 'company',
        loadChildren: () => import('../features/company').then(m => m.CompanyModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
