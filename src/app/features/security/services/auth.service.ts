import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Store} from "@ngrx/store";
import {selectConfigBaseUri} from "../../../shared/config/store/config.selector";
import {filter, map, shareReplay, switchMap} from "rxjs";
import {selectSecurityUser, selectSecurityUserAndBaseUri} from "../store/security.selector";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private $http: HttpClient,
    private $store: Store
  ) { }

  signIn(username: string, password: string) {
    return this.$store.select(selectConfigBaseUri).pipe(
      switchMap(uri => this.$http.post<{ accessToken: string, user: any }>(`${uri}/signIn`, { email: username, password })),
      shareReplay(),
      map(({accessToken, user}) => ({ token: accessToken, user }))
    )
  }

  getRoles() {
    return this.$store.select(selectSecurityUserAndBaseUri).pipe(
      filter(({ userId, uri}) => userId != null),
      switchMap(({uri, userId}) => this.$http.get<any[]>(`${uri}/roles?userId=${userId}`))
    )
  }
}
