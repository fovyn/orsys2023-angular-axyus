import {FormControl, Validators} from "@angular/forms";
import {TypedGroup} from "../../../shared/typed-form/models/typed-group";
import {domain, same} from "../../../shared/typed-form/validators/same.validator";

export type FSignIn = {
  email: FormControl,
  password: FormControl
}

export type FRegister = {
  email: FormControl,
  password: FormControl,
  verifPassword: FormControl
}

export function fSignIn(): TypedGroup<FSignIn> {
  return new TypedGroup({
    email: new FormControl('', [Validators.email]),
    password: new FormControl()
  })
}

export function fRegister(): TypedGroup<FRegister> {
  return new TypedGroup<FRegister>({
    email: new FormControl('', [Validators.email]),
    password: new FormControl('', [Validators.min(0)]),
    verifPassword: new FormControl('')
  }, [ same<FRegister>('password', 'verifPassword')])
}
