import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StoreModule} from "@ngrx/store";
import {CONFIG_FEATURE} from "./store/config.state";
import {configReducer} from "./store/config.reducer";
import {EffectsModule} from "@ngrx/effects";
import {ConfigEffect} from "./store/config.effect";

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature(CONFIG_FEATURE, configReducer),
    EffectsModule.forFeature([ConfigEffect])
  ]
})
export class ConfigModule { }
