import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigState} from "../store/config.state";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private $http: HttpClient) { }

  load(endpoint: string) {
    return this.$http.get<ConfigState>(`${endpoint}`)
  }
}
