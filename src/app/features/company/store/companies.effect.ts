import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {CompanyService} from "../services/company.service";
import {CompaniesActions} from "./companies.action";
import {catchError, delay, map, switchMap} from "rxjs";
import {Company} from "../models/company.model";

@Injectable()
export class CompaniesEffect {

  $initAction = createEffect(() => this.$action.pipe(
    ofType(CompaniesActions.init),
    map(() => CompaniesActions.load({ page: 1, limit: 5 }))
  ))

  $loadAction = createEffect(() => this.$action.pipe(
    ofType(CompaniesActions.load),
    switchMap(({page, limit}) => this.$company.getAll(page, limit)),
    map((data) => CompaniesActions.loadSuccess({data: data.data, total: data.total}))
  ))



  $removeAction = createEffect(() => this.$action.pipe(
    ofType(CompaniesActions.remove),
    switchMap(({id}) => this.$company.remove(id)),
    map((data) => CompaniesActions.removeSuccess())
  ))



  $createAction = createEffect(() => this.$action.pipe(
    ofType(CompaniesActions.create),
    switchMap(({company}) => this.$company.create(company)),
    map((company) => CompaniesActions.createSuccess({company}))
  ))

  constructor(
    private $action: Actions,
    private $company: CompanyService
  ) {
  }
}
