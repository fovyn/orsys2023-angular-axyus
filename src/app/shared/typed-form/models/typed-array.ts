import {TypedFormDeclaration} from "../typed-form.type";
import {FormArray} from "@angular/forms";
import {TypedGroup} from "./typed-group";

export class TypedArray<T extends TypedFormDeclaration> extends FormArray implements Iterable<TypedGroup<T>> {

  [Symbol.iterator](): Iterator<TypedGroup<T>> {
    let index = 0
    const $this = this;
    return {
      next: () => ({
        done: index >= $this.length,
        value: $this.at(index++) as TypedGroup<T>
      })
    }
  }
}
